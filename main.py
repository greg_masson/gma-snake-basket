from monkey.ioc.core import Registry

registry = Registry()
registry.load('config.json')
my_object = registry.get('myObjectID')